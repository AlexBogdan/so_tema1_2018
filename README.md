-=-=-=-=-=-= Andrei Bogdan Alexandru -=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-= 336 CB =-=-=-=-=-=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-Tema 1 SO -=-=-=-=-=-=-=-=-=-

	Pentru a implementa HashTable-ul am ales sa creez
3 structuri : Entry, Bucket si HashTable
	HashTable va fi reprezentat de o lista de Bucket-uri
	Fiecare Bucket va fi reprezentat de o lista simplu
inlantuita, avand un pointer catre baza acesteia (basE) si 
unul catre finalul acesteia (tail)

	In functia main testam parametrii primiti si in functie
de numarul lor vom citi de la tastatura sau dintr-o lista de 
fisiere.

	Pentru fiecare comanda citita (stdin sau fisier) vom
realiza parsarea acesteia si in functie de tipul comenzii vom
efectua cerinta.

	Un cuvant va fi adaugat in hashtable (la finalul
bucket-ului din care face parte) daca nu exisa deja.
	Pentru a cauta un cuvant in table, ii calculam
valoarea hash si parcurgem bucket-ul, intorcand true daca
cuvantul a fost gasit.
	Eliminarea unui cuvant se va realiza asemanator cu
cautarea unui cuvant, stergand nodul respectiv si legand
celalalte noduri din lista in lipsa lui.
	Clear se va realiza prin stergerea continutului bucket-urilor.
	Print Bucket va parcurge lista de cuvinte a bucket-ului
selectat si le va afisa pe rand daca nu era gol.
	Print Table va parcurge hashtable si va apela print_bucket
pentru fiecare bucket.
	Pentru resize vom crea un hashtable nou in care vom adauga
cuvintele din vechiul bucket si care va inlocui vechiul hashtable
transmis prin referinta.
	
	Fiecare fisier cu comenzi (sau input de la stdin) va fi parcurs
linie cu linie si parsata. In cazul in care intalnim o comanda
invalida, vom iesi cu DIE din program.