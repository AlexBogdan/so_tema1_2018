/* Autor : Andrei Bogdan Alexandru */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "hash.h"

#include <errno.h>
#define DIE(assertion, call_description)	\
		do {	\
			if (assertion) {	\
				fprintf(stderr, "(%s, %d): ",	\
						__FILE__, __LINE__);	\
				perror(call_description);	\
				exit(errno);	\
			}	\
		} while (0)

#define false 0
#define true !false
#define bool int
#define BUFSIZE 20000

typedef struct entry {
	char *value;
	struct entry *next;
} entry;

typedef struct bucket {
	struct entry *base;
	struct entry *tail;
} bucket;

typedef struct hashtable {
	unsigned int size;
	struct bucket *buckets;
} hashtable;

bool isnumber(const char *number);

entry *create_entry(const char *value);
void destroy_entry(entry *e);

bool empty_bucket(hashtable *h, int i);

hashtable *create_table(int size);
void insert_table(hashtable *h, const char* value);
void remove_table(hashtable *h, const char *value);
bool find_table(hashtable *h, const char *value, FILE *output_file);
void clear_table(hashtable *h);
void print_bucket(hashtable *h, int index, FILE *output_file);
void print_table(hashtable *h, FILE *output_file);
void resize_table(hashtable **h, int new_size);
void resize_double(hashtable **h);
void resize_halve(hashtable **h);
void destroy_table(hashtable *h);

int cmd_type(const char *command);
void parse_command(hashtable **h, char *command);
void read_input_file(hashtable *h, const char *filename);


bool empty_bucket(hashtable *h, int i)
{
	return h->buckets[i].base == NULL;
}

entry *create_entry(const char *value)
{
	entry *e;
	int len;

	len = strlen(value);

	e = malloc(sizeof(entry)+1);
	e->value = malloc(len+1);
	memcpy(e->value, value, len+1);
	e->value[len] = 0;
	e->next = NULL;

	return e;
}

void destroy_entry(entry *e)
{
	free(e->value);
	free(e);
}

hashtable *create_table(int size)
{
	int i;
	hashtable *h;

	h = malloc(sizeof(hashtable));
	h->size = size;

	h->buckets = malloc(sizeof(bucket) * size);
	for (i = 0; i < size; i++)
		h->buckets[i].base = NULL;

	return h;
}

void insert_table(hashtable *h, const char *value)
{
	unsigned int hash_value;

	/* Daca cuvantul este deja in Hashtable, nu vom adauga duplicate */
	if (find_table(h, value, NULL) == true)
		return;

	hash_value = hash(value, h->size);

	/* Daca bucketul este gol, atunci adaugam noul cuvant */
	if (h->buckets[hash_value].base == NULL) {
		h->buckets[hash_value].base = create_entry(value);
		h->buckets[hash_value].tail = h->buckets[hash_value].base;
		return;
	}

	/* Cuvantul nu a fost gasit, il adaugam la finalul Bucket-ului */
	h->buckets[hash_value].tail->next = create_entry(value);
	h->buckets[hash_value].tail = h->buckets[hash_value].tail->next;
}

void remove_table(hashtable *h, const char *value)
{
	int hash_value;
	entry *it, *temp_it;

	if (find_table(h, value, NULL) == false)
		return;

	hash_value = hash(value, h->size);

	it = h->buckets[hash_value].base;
	if (strcmp(it->value, value) == 0) {
		h->buckets[hash_value].base = it->next;
		if (it == h->buckets[hash_value].tail)
			h->buckets[hash_value].tail = NULL;
		destroy_entry(it);
		return;
	}

	for ( ; it->next != NULL; it = it->next) {
		if (strcmp(it->next->value, value) == 0) {
			if (h->buckets[hash_value].tail == it->next)
				h->buckets[hash_value].tail = it;
			temp_it = it->next;
			it->next = it->next->next;
			destroy_entry(temp_it);
			return;
		}
	}
}

bool find_table(hashtable *h, const char *value, FILE *output_file)
{
	/* Ne pozitionam la inceputul bucket-ului */
	/* corespunzator cuvantului primit */
	int hash_value;
	entry *it;
	bool result;

	hash_value = hash(value, h->size);
	result = false;
	/* Verificam daca cuvantul se afla deja in HashTable */
	for (it = h->buckets[hash_value].base; it != NULL; it = it->next)
		if (strcmp(it->value, value) == 0) {
			/* Am gasit acest cuvant in HashTable  */
			result = true;
			break;
		}
	/* Am terminat bucket-ul si nu am gasit cuvantul */

	if (output_file != NULL) {
		if (result == true)
			fprintf(output_file, "True\n");
		else
			fprintf(output_file, "False\n");
	}
	return result;
}

void clear_table(hashtable *h)
{
	int i;
	entry *it1;
	entry *it2;

	for (i = 0; i < h->size; i++) {
		for (it1 = h->buckets[i].base; it1 != NULL; it1 = it2) {
			it2 = it1->next;
			destroy_entry(it1);
		}
		h->buckets[i].base = NULL;
		h->buckets[i].tail = NULL;
	}
}

void print_bucket(hashtable *h, int index, FILE *output_file)
{
	entry *it;

	if (index > h->size || empty_bucket(h, index) == true)
		return;
	for (it = h->buckets[index].base; it != NULL; it = it->next)
		fprintf(output_file, "%s ", it->value);
	fprintf(output_file, "\n");
}

void print_table(hashtable *h, FILE *output_file)
{
	int i;

	for (i = 0; i < h->size; i++)
		print_bucket(h, i, output_file);
	fprintf(output_file, "\n");
}

void resize_table(hashtable **h, int new_size)
{
	hashtable *new_hashtable;
	entry *it;
	int i;

	/* Nu putem crea un tabel cu aceasta dimensiune */
	if (new_size < 1)
		return;

	new_hashtable = create_table(new_size);
	for (i = 0; i < (*h)->size; i++)
		for (it = (*h)->buckets[i].base; it != NULL; it = it->next)
			insert_table(new_hashtable, it->value);

	destroy_table(*h);

	*h = new_hashtable;
}

void resize_double(hashtable **h)
{
	resize_table(h, (*h)->size * 2);
}

void resize_halve(hashtable **h)
{
	resize_table(h, (*h)->size / 2);
}

void destroy_table(hashtable *h)
{
	clear_table(h);
	free(h->buckets);
	free(h);
}

int cmd_type(const char *command)
{
	if (command == NULL || strlen(command) == 0)
		return -2;
	if (strcmp(command, "add") == 0)
		return 1;
	if (strcmp(command, "remove") == 0)
		return 2;
	if (strcmp(command, "find") == 0)
		return 3;
	if (strcmp(command, "clear") == 0)
		return 4;
	if (strcmp(command, "print_bucket") == 0)
		return 5;
	if (strcmp(command, "print") == 0)
		return 6;
	if (strcmp(command, "resize") == 0)
		return 7;
	return -1;
}

void parse_command(hashtable **h, char *command)
{
	FILE *output_file;
	char *token;
	char *cmd = command;
	char *cmd_parts[3] = {NULL};
	const char delim[2] = " ";
	int i, type;

	if (cmd[strlen(cmd)-1] == '\n')
		cmd[strlen(cmd)-1] = 0;

	token = strtok(cmd, delim);
	for (i = 0; i < 3 && token != NULL; ++i) {
		cmd_parts[i] = malloc(sizeof(char) * strlen(token)+1);
		strcpy(cmd_parts[i], token);
		token = strtok(NULL, delim);
	}

	/* Daca linia a fost goala, o sarim */
	if (cmd_parts[0] == NULL)
		return;

	type = cmd_type(cmd_parts[0]);
	switch (type) {
	case 1:
		DIE(cmd_parts[1] == NULL, "Comanda eronata!");
		insert_table(*h, cmd_parts[1]);
		break;
	case 2:
		DIE(cmd_parts[1] == NULL, "Comanda eronata!");
		remove_table(*h, cmd_parts[1]);
		break;
	case 3:
		if (cmd_parts[2] != NULL) {
			output_file = fopen(cmd_parts[2], "a");
			find_table(*h, cmd_parts[1], output_file);
			fclose(output_file);
		} else
			find_table(*h, cmd_parts[1], stdout);
		break;
	case 4:
		clear_table(*h);
		break;
	case 5:
		if (cmd_parts[1] != NULL && isnumber(cmd_parts[1]) == true) {
			if (cmd_parts[2] != NULL) {
				output_file = fopen(cmd_parts[2], "a");
				print_bucket(*h, atoi(cmd_parts[1]),
					output_file);
				fclose(output_file);
			} else {
				print_bucket(*h, atoi(cmd_parts[1]), stdout);
			}
		}
		break;
	case 6:
		if (cmd_parts[1] != NULL) {
			output_file = fopen(cmd_parts[1], "a");
			print_table(*h, output_file);
			fclose(output_file);
		} else
			print_table(*h, stdout);
		break;
	case 7:
		if (strcmp(cmd_parts[1], "double") == 0)
			resize_double(h);
		if (strcmp(cmd_parts[1], "halve") == 0)
			resize_halve(h);
		break;
	default:
		DIE(true, "Comanda eronata!");
		break;
	}

	for (i = 0; i < 3; ++i)
		if (cmd_parts[i] != NULL)
			free(cmd_parts[i]);
}

void read_input(hashtable **h, FILE *input_file)
{
	char buf[BUFSIZE+100], *result;

	while (1) {
		result = fgets(buf, BUFSIZE, input_file);
		if (result == NULL)
			break;
		parse_command(h, buf);
	}
}

bool isnumber(const char *number)
{
	int i, len;

	len = strlen(number);
	for (i = 0; i < len; ++i)
		if (isdigit(number[i]) == 0)
			return false;
	return true;
}

int main(int argc, char **argv)
{
	int file_index;
	hashtable *h;
	FILE *input_file;

	DIE(argc <= 1, "./tema1 [HashTable Size]\n");
	DIE(isnumber(argv[1]) == false, "Size should be a number!\n");
	DIE(atoi(argv[1]) > 65535, "Size should less than 65535!\n");

	h = create_table(atoi(argv[1]));

	/* Nu avem alte fisiere, citim de la stdin */
	if (argc == 2)
		read_input(&h, stdin);

	/* Dechidem fiecare fisier in parte si executam comenziile */
	for (file_index = 2; file_index < argc; file_index++) {
		input_file = fopen(argv[file_index], "r");
		read_input(&h, input_file);
		fclose(input_file);
	}

	destroy_table(h);

	return 0;
}
